#!perl

use strict;

use Cwd;
use File::Basename;
use Getopt::Long;
use JSON qw(encode_json decode_json);
use LWP::UserAgent;
use HTTP::Request::Common qw(GET POST DELETE);
use Data::Dumper qw(Dumper);
use Term::ReadKey;
use HTML::Parser;

my $credentials = {
  username => undef,
  password => undef
};

my %options = (
  scm => "git",
  public => 0,
  remote => "origin",
  username => $ENV{BB_USERNAME} || $ENV{USER} || $ENV{USERNAME},
  password => $ENV{BB_PASSWORD},
  verbose => 0
);
GetOptions(\%options,
  "scm|s:s",
  "public",
  "remote|r:s",
  "username|u:s",
  "password|p:s",
  "verbose|v"
);

my $action = shift(@ARGV);

if ($action eq "create") {
  bb_create();
} elsif ($action eq "import") {
  bb_import();
} elsif ($action eq "delete") {
  bb_delete();
} elsif ($action eq "clone") {
  bb_clone();
} elsif ($action eq "list") {
  bb_list();
} elsif ($action eq "update") {
  bb_update();
} elsif ($action eq "tags") {
  bb_tags();
} elsif ($action eq "branches") {
  bb_branches();
} else {
  usage();
}

sub usage {
print <<END
Usage: 
  bb <action> [args]

Actions:
  create
  delete
  import
  clone
  list
  update
  tags
  branches
END
}

sub strip_html {
  my ($s) = @_;
  # From perlfaq9: How do I remove HTML from a string?
  $s =~ s/<ul/\n<ul/g;
  $s =~ s/<li/\n<li/g;
  $s =~ s/<(?:[^>'"]*|(['"]).*?\g1)*>//gs;
  return $s;
}

sub get_credentials {
  my $input_requested;
  if (!defined($credentials->{username})) {
    if ($options{username} ne "") {
      $credentials->{username} = $options{username};
      #print "Username: ", $credentials->{username}, "\n";
    } else {
      do {
        $input_requested = 1;
        print "Username: ";
        $credentials->{username} = <STDIN>;
        chomp($credentials->{username});
        $credentials->{username} =~ s/(^\s+)|(\s+$)//g;
      } while ($credentials->{username} eq "");
    }
  }
  if (!defined($credentials->{password})) {
    if ($options{password} ne "") {
      $credentials->{password} = $options{password};
    } else {
      do {
        $input_requested = 1;
        print "Password for $credentials->{username}: ";
        ReadMode('noecho');
        $credentials->{password} = <STDIN>;
        print "\n";
        ReadMode('restore');
        chomp($credentials->{password});
        $credentials->{password} =~ s/(^\s+)|(\s+$)//g;
      } while ($credentials->{password} eq "");
    }
  }
  if ($input_requested) {
    print "\n";
  }
  return ($credentials->{username}, $credentials->{password});
}

sub get_extra {
  my ($slug, $extra) = @_;

  if (! grep { $_ eq $extra } qw/tags branches/) {
    return undef;
  }

  my ($username, $password) = get_credentials();
  return undef unless defined($username);
  
  my $url = "https://$username:$password\@api.bitbucket.org/1.0/repositories/$username/$slug/$extra/";

  my $request = GET $url;

  my $ua = new LWP::UserAgent;
  $ua->timeout(10);
  
  my $response = $ua->request($request);

  if ($response->is_success) {
    my $json = decode_json($response->decoded_content);
    return $json;
  } else {
    print STDERR "ERROR looking up slug '$slug': ", $response->status_line, "\n";
    my $msg = strip_html($response->decoded_content);
    print STDERR $msg, "\n";
    return undef;
  }
}

sub get_info {
  my ($name, $extra) = @_;

  my ($username, $password) = get_credentials();
  return undef unless defined($username);
  
  my $url = "https://$username:$password\@api.bitbucket.org/1.0/user/repositories/";

  my $request = GET $url;

  my $ua = new LWP::UserAgent;
  $ua->timeout(10);
  
  #print "Looking up repository '$name'\n";
  my $response = $ua->request($request);

  if ($response->is_success) {
    my $json = decode_json($response->decoded_content);
    if ($name ne "") {
      foreach my $repository (@$json) {
        if (lc($repository->{name}) eq lc($name) || lc($repository->{slug}) eq lc($name)) {
          return ($repository, get_extra($repository->{slug}, $extra));
        }
      }
      return undef;
    } else {
      my @repositories;
      push @repositories, $_ foreach (@$json);
      return @repositories;
    }
  } else {
    print STDERR "ERROR looking up repository '$name': ", $response->status_line, "\n";
    my $msg = strip_html($response->decoded_content);
    print STDERR $msg, "\n";
    return undef;
  }
}

sub bb_branches {
  my $name = shift(@ARGV);
  if (!defined($name) || ($name eq "")) {
    my $git_branch = `git branch`;
    $name = basename(getcwd()) unless $?;
  }
  
  if ($name eq "." || $name eq "") {
    print STDERR "Missing required argument: name\n";
    return undef;
  }
  
  my ($username, $password) = get_credentials();
  return undef unless defined($username);

  my ($info, $branches) = get_info($name, 'branches');
  foreach my $branch (keys %$branches) {
    #print Dumper($branch);
    print $branch, "\n";
    if ($options{verbose}) {
      my @keys = keys(%{$branches->{$branch}});
      foreach my $key (@keys) {
        my $print_key = $key;
        $print_key =~ s/[_]/ /g;
        $print_key =~ s/(^| )(\w)/$1\U$2/g;
        my $value = $branches->{$branch}->{$key};
        my $print_value = ref($value) ? Dumper($value) : $value;
        print "  ", $print_key, ": ", $print_value, "\n";
      }
      print "\n" if (@keys);
    }
  }
  
  return 1;
}

sub bb_tags {
  my $name = shift(@ARGV);
  if (!defined($name) || ($name eq "")) {
    my $git_branch = `git branch`;
    $name = basename(getcwd()) unless $?;
  }
  
  if ($name eq "." || $name eq "") {
    print STDERR "Missing required argument: name\n";
    return undef;
  }
  
  my ($username, $password) = get_credentials();
  return undef unless defined($username);

  my ($info, $tags) = get_info($name, 'tags');
  foreach my $tag (keys %$tags) {
    #print Dumper($tag);
    print $tag, "\n";
    if ($options{verbose}) {
      my @keys = keys(%{$tags->{$tag}});
      foreach my $key (@keys) {
        my $print_key = $key;
        $print_key =~ s/[_]/ /g;
        $print_key =~ s/(^| )(\w)/$1\U$2/g;
        my $value = $tags->{$tag}->{$key};
        my $print_value = ref($value) ? Dumper($value) : $value;
        print "  ", $print_key, ": ", $print_value, "\n";
      }
      print "\n" if (@keys);
    }
  }
  
  return 1;
}

sub bb_list {
  my @repositories = get_info();
  
  foreach my $repository (@repositories) {
    print $repository->{name}, "\n";
    if ($options{verbose}) {
      foreach my $key (keys(%$repository)) {
        my $print_key = $key;
        $print_key =~ s/[_]/ /g;
        $print_key =~ s/ (.)/ \u$1/g;
        print "  ", $print_key, ": ", $repository->{$key}, "\n";
      }
    }
  }

  return 1;
}

sub bb_clone {
  my $name = shift(@ARGV);
  
  if ($name eq "." || $name eq "") {
    print STDERR "Missing required argument: name\n";
    return undef;
  }
  
  my ($username, $password) = get_credentials();
  return undef unless defined($username);

  my ($info) = get_info($name);
  if (!$info) {
    print "ERROR: Cannot find info for repository '$name'\n";
    return undef;
  }
  my $slug = $info->{slug};
  my $target = shift(@ARGV) || $slug;

  print "Cloning repository '$name' into $target\n";
  `git clone git\@bitbucket.org:$username/$slug.git $target`;
  if ($?) {
    print "ERROR cloning '$name': $?\n";
    return undef;
  }

  return 1;
}

sub bb_delete {
  my $name = shift(@ARGV);
  if (!defined($name) || ($name eq "")) {
    my $git_branch = `git branch`;
    $name = basename(getcwd()) unless $?;
  }

  if ($name eq "." || $name eq "") {
    print STDERR "Missing required argument: name\n";
    return undef;
  }

  my ($username, $password) = get_credentials();
  return undef unless defined($username);

  my ($info) = get_info($name);
  if (!$info) {
    print "ERROR: Cannot find info for repository '$name'\n";
    return undef;
  }

  print "Are you sure you want to delete repository '$name'? (y/N) ";
  my $yn = <STDIN>;
  chomp($yn);
  if (lc($yn) ne "y") {
    return 1;
  }
  
  my $slug = $info->{slug};
  my $url = "https://$username:$password\@api.bitbucket.org/1.0/repositories/$username/$slug/";

  my $request = DELETE $url;

  my $ua = new LWP::UserAgent;
  $ua->timeout(10);

  my $git_remote = `git remote -v`;
  unless ($?) {
    my @remote_match;
    my @lines = split(/\n/, $git_remote);
    my $repo_url = "git\@bitbucket.org:$username/$slug.git";
    foreach my $line (@lines) {
      my ($remote, $url, $type, $other) = split(/[\t\s]+/, $line, 4);
      if ((! grep { $_ eq $remote } @remote_match) && ($url eq $repo_url)) {
        push @remote_match, $remote;
        print "Remove remote '$remote'? (y/N) ";
        my $yn = <STDIN>;
        chomp($yn);
        if (lc($yn) eq "y") {
          `git remote rm $remote`;
          if ($?) {
            print STDERR "ERROR removing remote '$remote'\n";
          }
        }
      }
    }
  }

  print "Deleting repository '$name'\n";
  my $response = $ua->request($request);

  if ($response->is_success) {
    my $json;
    if ($response->decoded_content) {
      eval {
        $json = decode_json($response->decoded_content);
      };
      if (!$json) {
        print $response->decoded_content;
      }
      print Dumper($json);
    }
    return $json;
  } else {
    print STDERR "ERROR: ", $response->status_line, "\n";
    my $msg = strip_html($response->decoded_content);
    print STDERR $msg, "\n";
    return undef;
  }
}

sub bb_create {
  my $name = shift(@ARGV);
  if (!defined($name) || ($name eq "")) {
    my $git_branch = `git branch`;
    $name = basename(getcwd()) unless $?;
  }

  if ($name eq "." || $name eq "") {
    print STDERR "Missing required argument: name\n";
    return undef;
  }

  my ($username, $password) = get_credentials();
  return undef unless defined($username);

  my $url = "https://$username:$password\@api.bitbucket.org/1.0/repositories/";
  my $form = [
    name => $name,
    scm => $options{scm},
    is_private => $options{public} ? "False" : "True"
  ];

  my $request = POST $url, $form;

  my $ua = new LWP::UserAgent;
  $ua->timeout(10);
  
  my $privpub = $options{public} ? "public" : "private";
  print "Creating $privpub repository '$name'\n";
  my $response = $ua->request($request);

  if ($response->is_success) {
    my $json = decode_json($response->decoded_content);
    return $json;
  } else {
    print STDERR "ERROR: ", $response->status_line, "\n";
    my $msg = strip_html($response->decoded_content);
    print STDERR $msg, "\n";
    return undef;
  }

  return 1;
}

sub bb_import {
  my $git_branch = `git branch`;
  if ($?) {
    print "ERROR: Git branch returned error";
    return undef;
  }

  my ($bb_info) = bb_create();
  return undef unless $bb_info;

  my $slug = $bb_info->{slug};
  my $remote = $options{remote};

  my ($username, $password) = get_credentials();
  return undef unless defined($username);

  print "Adding remote '$remote'\n";
  `git remote add $remote git\@bitbucket.org:$username/$slug.git`;
  if ($?) {
    print "ERROR adding remote '$remote': $?\n";
    return undef;
  }

  print "Pushing current branch to remote '$remote'\n";
  `git push $remote HEAD`;
  if ($?) {
    print "ERROR pushing to remote '$remote': $?\n";
    return undef;
  }

  return 1;
}

